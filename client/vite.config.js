import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/expeditions/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "expeditions",
      name: "Gamesite6_expeditions",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
