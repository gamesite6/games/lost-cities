/// <reference types="svelte" />
/// <reference types="vite/client" />

type PlayerId = number;

type CardId = number;

type GameState = {
  players: PlayerState[];
  deck: CardId[];
  discarded: CardId[];
  phase: Phase;
};

type PlayerState = {
  id: PlayerId;
  hand: CardId[];
  placed: CardId[];
};

type Phase =
  | { k: "Placing"; v: { player: PlayerId } }
  | { k: "Drawing"; v: { player: PlayerId; discarded: CardId | null } }
  | { k: "Complete"; v: [] };

type GameSettings = { sixColors: boolean };

type Action =
  | { k: "Place"; v: CardId }
  | { k: "Discard"; v: CardId }
  | { k: "Take"; v: Color }
  | { k: "Draw"; v: [] };
