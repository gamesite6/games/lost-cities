import { Color, getCard } from "./game";
import { rangeInclusive } from "./utils";
import { describe, test, expect } from "vitest";

describe("get card from card id", () => {
  test("correct number of (red) cards", () => {
    let cardIds = rangeInclusive(1, 60);
    let cards = cardIds.map(getCard);

    let redCards = cards.filter((c) => c.color === Color.Red);
    expect(redCards.length).toBe(12);
    let redWagerCards = redCards.filter((c) => c.value === "x");
    expect(redWagerCards.length).toBe(3);
  });

  test("first value is 2", () => {
    let cardIds = rangeInclusive(1, 60);
    let cards = cardIds.map(getCard);

    let valueCard = cards.find((c) => c.value !== "x");
    expect(valueCard?.value).toBe(2);
  });

  test("60th card is red 10", () => {
    let card = getCard(60);
    expect(card.color).toBe(Color.Red);
    expect(card.value).toBe(10);
  });
});
