import type { Color } from "./game";

export function range(from: number, to: number): number[] {
  const size = Math.max(to - from, 0);
  const result = new Array(size);

  for (let i = 0; i < size; i++) {
    result[i] = from + i;
  }

  return result;
}

export function rangeInclusive(from: number, to: number): number[] {
  return range(from, to + 1);
}

export function reversed<T>(ts: T[]): T[] {
  let result = [...ts];
  result.reverse();
  return result;
}

export function take<T>(n: number, ts: T[]): T[] {
  let result = [];
  for (let i = 0; i < n && i < ts.length; i++) {
    result[i] = ts[i];
  }
  return result;
}

export function indexed<T>(ts: T[]): [T, number][] {
  return ts.map((t, i) => [t, i]);
}

export function sorted(cards: CardId[]): CardId[] {
  let result = [...cards];
  result.sort((a, b) => a - b);
  return result;
}

export function last<T>(ts: T[]): T | undefined {
  return ts[ts.length - 1];
}

/**
 * Reorder players so the user comes last
 */
export function orderPlayers(
  players: PlayerState[],
  userId: PlayerId | null
): PlayerState[] {
  if (players[0].id === userId) {
    return reversed(players);
  } else {
    return players;
  }
}
