import { sorted } from "./utils";

export enum Color {
  Yellow = "Yellow",
  Blue = "Blue",
  White = "White",
  Green = "Green",
  Red = "Red",
  Purple = "Purple",
}

export function colors(settings: GameSettings): Color[] {
  let result = [Color.Yellow, Color.Blue, Color.White, Color.Green, Color.Red];
  if (settings.sixColors) {
    result.push(Color.Purple);
  }
  return result;
}

export type GroupedCards = { [color in Color]: ExpeditionCard[] };
export function groupCards(cardIds: CardId[]): GroupedCards {
  const result: GroupedCards = {
    Yellow: [],
    Blue: [],
    White: [],
    Green: [],
    Red: [],
    Purple: [],
  };

  for (let cardId of cardIds) {
    const card = getCard(cardId);
    result[card.color].push(card);
  }

  return result;
}

export function groupCardsSorted(cardIds: CardId[]): GroupedCards {
  const result: GroupedCards = {
    Yellow: [],
    Blue: [],
    White: [],
    Green: [],
    Red: [],
    Purple: [],
  };

  cardIds = sorted(cardIds);

  for (let cardId of cardIds) {
    const card = getCard(cardId);
    result[card.color].push(card);
  }

  return result;
}

type ExpeditionCard = {
  id: CardId;
  color: Color;
  value: CardValue;
};

type CardValue = "x" | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

export function getCard(cardId: CardId): ExpeditionCard {
  return {
    id: cardId,
    color: getCardColor(cardId),
    value: getCardValue(cardId),
  };
}

function getCardColor(cardId: CardId): Color {
  switch (Math.floor((cardId - 1) / 12)) {
    case 0:
      return Color.Yellow;
    case 1:
      return Color.Blue;
    case 2:
      return Color.White;
    case 3:
      return Color.Green;
    case 4:
      return Color.Red;
    default:
      return Color.Purple;
  }
}

function getCardValue(cardId: CardId): CardValue {
  let value = ((cardId - 1) % 12) - 1;
  if (value < 2) {
    return "x";
  } else {
    return value as CardValue;
  }
}

export function isPlayerActive(state: GameState, playerId: PlayerId): boolean {
  switch (state.phase.k) {
    case "Placing":
      return state.phase.v.player === playerId;
    case "Drawing":
      return state.phase.v.player === playerId;
    case "Complete":
      return false;
  }
}

export function getActivePlayerId(state: GameState): PlayerId | null {
  switch (state.phase.k) {
    case "Placing":
      return state.phase.v.player;
    case "Drawing":
      return state.phase.v.player;
    case "Complete":
      return null;
  }
}

export function colorScore(
  player: PlayerState,
  color: Color
): { normal: number; bonus: number } {
  let groups = groupCards(player.placed);
  let cards = groups[color];

  if (cards.length === 0) return { normal: 0, bonus: 0 };

  let wagers = cards.filter((c) => c.value === "x");
  let multiplier = 1 + wagers.length;

  let values = cards
    .filter((c) => c.value !== "x")
    .map((c) => c.value as number);

  let sum = values.reduce((a, b) => a + b, 0);

  let normal = (sum - 20) * multiplier;
  let bonus = cards.length >= 8 ? 20 : 0;

  return { normal, bonus };
}

export function totalScore(player: PlayerState): number {
  let colors = new Set(player.placed.map(getCardColor));

  let total = 0;
  for (let color of colors) {
    let { normal, bonus } = colorScore(player, color);
    total += normal + bonus;
  }
  return total;
}
