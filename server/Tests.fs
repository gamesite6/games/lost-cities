namespace Game

open NUnit.Framework
open FsUnit

module ``get card from card id`` =
    let cards = [ 1..60 ] |> List.map Card.getCard

    [<Test>]
    let ``Correct number of red cards`` () =
        let redCards = cards |> List.filter (fun c -> c.color = Red)

        redCards.Length |> should equal 12

    [<Test>]
    let ``Lowest card value is 2`` () =
        let cardsWithValue =
            cards
            |> List.choose (fun c ->
                match c with
                | Expedition c -> Some c
                | Multiplier _ -> None)

        let minValue =
            cardsWithValue
            |> List.map (fun c -> c.value)
            |> List.min

        minValue |> should equal 2

    [<Test>]
    let ``60th card is Red 10`` () =
        let card = Card.getCard 60

        card
        |> should equal (Expedition { color = Red; value = 10 })

module ``phase json`` =
    open FSharp.Json

    [<Test>]
    let ``'Complete' phase is JSON object`` =
        let json = Json.serializeU <| (Complete { winners = [] })

        json
        |> should equal """{"k":"Complete","v":{"winners":[]}}"""
