#!/bin/bash

dotnet publish \
  --configuration Release \
  --output ./out
