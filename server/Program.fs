namespace Game

open System
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting


module Program =
    let exitCode = 0

    let CreateHostBuilder args (port: uint16) =
        Host
            .CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(fun webBuilder ->
                webBuilder.UseStartup<Startup>() |> ignore
                webBuilder.UseUrls($"http://*:{port}") |> ignore)

    [<EntryPoint>]
    let main args =

        let port =
            try
                Environment.GetEnvironmentVariable "PORT"
                |> uint16
            with
            | _ -> failwith "invalid PORT env var"

        (CreateHostBuilder args port).Build().Run()

        exitCode
