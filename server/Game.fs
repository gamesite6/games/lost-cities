namespace Game

open System
open FSharp.Json

type PlayerId = int

type Settings = { sixColors: bool }

type Color =
    | Yellow
    | Blue
    | White
    | Green
    | Red
    | Purple

module Color =
    let colors =
        [ Yellow
          Blue
          White
          Green
          Red
          Purple ]

type CardId = int

type ExpeditionCard = { color: Color; value: int }





module Deck =
    let create (sixColors: bool) (rng: Random) : CardId list =
        let cardCount = if sixColors then 72 else 60

        let arr = [| 1..cardCount |]
        Utils.Array.shuffle arr rng ()

        List.ofArray arr


type Card =
    | Expedition of ExpeditionCard
    | Multiplier of color: Color

    member this.color: Color =
        match this with
        | Expedition card -> card.color
        | Multiplier color -> color

module Card =
    let getCard (cardId: CardId) : Card =
        let color = Color.colors.[(cardId - 1) / 12]
        let value = ((cardId - 1) % 12) - 1

        if value < 2 then
            Multiplier color
        else
            Expedition { color = color; value = value }

type PlayerState =
    { id: PlayerId
      hand: CardId list
      placed: CardId list }



type Placing = { player: PlayerId }

type Drawing =
    { player: PlayerId
      discarded: CardId option }

type Complete = { winners: PlayerId list }

[<JsonUnion(Mode = UnionMode.CaseKeyAsFieldValue, CaseKeyField = "k", CaseValueField = "v")>]
type Phase =
    | Placing of Placing
    | Drawing of Drawing
    | Complete of Complete


[<JsonUnion(Mode = UnionMode.CaseKeyAsFieldValue, CaseKeyField = "k", CaseValueField = "v")>]
type Action =
    | Place of CardId
    | Discard of CardId
    | Draw
    | Take of Color

type GameState =
    { players: PlayerState list
      deck: CardId list
      discarded: CardId list
      phase: Phase }

    member this.isComplete =
        match this.phase with
        | Complete _ -> true
        | _ -> false

module GameState =
    let initialState (settings: Settings) (playerIds: PlayerId list) (seed: int64) : GameState option =
        let rng = Random((int) seed)
        let playerIdSet = Set.ofList playerIds

        if playerIdSet.Count <> playerIds.Length
           && playerIds.Length <> 2 then
            None
        else
            let mutable deck = Deck.create settings.sixColors rng

            let playerIds = Array.ofList playerIds
            Utils.Array.shuffle playerIds rng ()
            let playerIds = List.ofArray playerIds

            let players =
                playerIds
                |> List.map (fun pid ->
                    let (hand, deck') = List.splitAt 8 deck
                    deck <- deck'

                    { id = pid; hand = hand; placed = [] })

            Some
                { players = players
                  deck = deck
                  discarded = []
                  phase = Placing { player = players.Head.id } }


    let isActionAllowed
        (settings: Settings)
        (previousState: GameState)
        (action: Action)
        (performedBy: PlayerId)
        : bool =
        match previousState.phase with
        | Placing phase ->
            match action with
            | Place cardId ->
                let player =
                    previousState.players
                    |> List.tryFind (fun p -> p.id = performedBy)

                match player with
                | None -> false
                | Some (player) ->

                    let playerHasCard = player.hand |> List.contains cardId
                    let card = Card.getCard cardId

                    let previousExpeditionCards =
                        player.placed
                        |> List.map Card.getCard
                        |> List.filter (fun c -> c.color = card.color)
                        |> List.choose (fun c ->
                            match c with
                            | Expedition v -> Some(v.value)
                            | Multiplier _ -> None)

                    let placementIsValid =
                        if previousExpeditionCards.IsEmpty then
                            true
                        else
                            match card with
                            | Multiplier _ -> false
                            | Expedition e ->
                                let previousMax = previousExpeditionCards |> List.max
                                e.value > previousMax

                    phase.player = performedBy
                    && playerHasCard
                    && placementIsValid

            | Discard cardId ->
                let player =
                    previousState.players
                    |> List.tryFind (fun p -> p.id = performedBy)

                match player with
                | None -> false
                | Some (player) ->
                    phase.player = performedBy
                    && player.hand |> List.contains cardId

            | _ -> false
        | Drawing phase ->
            match action with
            | Draw -> phase.player = performedBy
            | Take color ->
                let discardedColorExists =
                    previousState.discarded
                    |> List.exists (fun c -> Card.getCard(c).color = color)

                phase.player = performedBy && discardedColorExists

            | _ -> false
        | Complete _ -> false

    let performAction
        (settings: Settings)
        (previousState: GameState)
        (action: Action)
        (performedBy: PlayerId)
        (seed: int64)
        : GameState option =
        if not
           <| isActionAllowed settings previousState action performedBy then
            None
        else
            match previousState.phase with
            | Placing _ ->
                match action with
                | Place placedCardId ->
                    Some(
                        { previousState with
                            players =
                                previousState.players
                                |> List.map (fun player ->
                                    if player.id = performedBy then
                                        { player with
                                            hand = List.filter ((<>) placedCardId) player.hand
                                            placed = placedCardId :: player.placed }
                                    else
                                        player)
                            phase =
                                Drawing
                                    { player = performedBy
                                      discarded = None } }
                    )
                | Discard discardedCardId ->
                    Some(
                        { previousState with
                            discarded = discardedCardId :: previousState.discarded
                            players =
                                previousState.players
                                |> List.map (fun player ->
                                    if player.id = performedBy then
                                        { player with hand = List.filter ((<>) discardedCardId) player.hand }
                                    else
                                        player)
                            phase =
                                Drawing
                                    { player = performedBy
                                      discarded = Some discardedCardId } }
                    )
                | _ -> None

            | Drawing phase ->
                let nextPlayer =
                    previousState.players
                    |> List.find (fun player -> player.id <> performedBy)

                match action with
                | Draw ->
                    let card = previousState.deck.Head
                    let remainingDeck = previousState.deck.Tail

                    Some(
                        { previousState with
                            players =
                                previousState.players
                                |> List.map (fun player ->
                                    if player.id = performedBy then
                                        { player with hand = card :: player.hand }
                                    else
                                        player)
                            deck = remainingDeck
                            phase =
                                if remainingDeck.IsEmpty then
                                    Complete
                                        { winners = [] // TODO
                                        }
                                else
                                    Placing { player = nextPlayer.id } }
                    )
                | Take takenColor ->
                    let takenCardId =
                        previousState.discarded
                        |> List.find (fun cardId -> (Card.getCard cardId).color = takenColor)

                    Some(
                        { previousState with
                            discarded =
                                previousState.discarded
                                |> List.filter ((<>) takenCardId)
                            players =
                                previousState.players
                                |> List.map (fun player ->
                                    if player.id = performedBy then
                                        { player with hand = takenCardId :: player.hand }
                                    else
                                        player)
                            phase = Placing { player = nextPlayer.id } }
                    )

                | _ -> None

            | Complete { winners = _winners } -> None
