namespace Utils

open System

module Array =
    let swap (a: _ []) x y () =
        let tmp = a.[x]
        a.[x] <- a.[y]
        a.[y] <- tmp

    let shuffle (a: _ []) (rng: Random) () =
        Array.iteri (fun i _ -> swap a i (rng.Next(i, Array.length a)) ()) a
