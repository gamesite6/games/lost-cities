﻿namespace Game

open FSharp.Json
open Microsoft.AspNetCore.Mvc
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Logging
open System.IO

type InfoRequest = { settings: Settings }

type InfoResponse = { playerCounts: int list }

type InitialStateRequest =
    { players: PlayerId list
      settings: Settings
      seed: int64 }

type InitialStateResponse = { state: GameState }

type PerformActionRequest =
    { performedBy: PlayerId
      action: Action
      state: GameState
      settings: Settings
      seed: int64 }

type PerformActionResponse =
    { completed: bool
      nextState: GameState }



[<ApiController>]
type GameController(logger: ILogger<GameController>) =
    inherit ControllerBase()

    [<HttpPost("info")>]
    [<Consumes("application/json")>]
    member this.Info() =
        let response: InfoResponse = { playerCounts = [ 2 ] }
        ContentResult(Content = Json.serializeU response, ContentType = "application/json; charset=utf-8")

    [<HttpPost("initial-state")>]
    [<Consumes("application/json")>]
    member this.InitialState() =
        let body = new StreamReader(this.Request.Body)
        let str = body.ReadToEndAsync().Result

        let req: InitialStateRequest option =
            try
                Json.deserialize str |> Some
            with
            | e ->
                logger.LogDebug(e, "json deserialization failed")
                None

        match req with
        | Some req ->
            match GameState.initialState req.settings req.players req.seed with
            | Some state ->
                let response: InitialStateResponse = { state = state }

                ContentResult(Content = Json.serializeU response, ContentType = "application/json; charset=utf-8")

            | None -> ContentResult(StatusCode = 422)
        | None -> ContentResult(StatusCode = 400)

    [<HttpPost("perform-action")>]
    member this.PerformAction() =
        let body = new StreamReader(this.Request.Body)
        let str = body.ReadToEndAsync().Result

        let req: PerformActionRequest option =
            try
                Json.deserialize str |> Some
            with
            | e ->
                logger.LogDebug(e, "json deserialization failed")
                None

        match req with
        | Some req ->
            match GameState.performAction req.settings req.state req.action req.performedBy req.seed with
            | Some state ->
                ContentResult(
                    Content =
                        Json.serializeU
                            { nextState = state
                              completed = state.isComplete },
                    ContentType = "application/json; charset=utf-8"
                )
            | None -> ContentResult(StatusCode = 422)
        | None -> ContentResult(StatusCode = 400)
